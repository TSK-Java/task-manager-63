package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.service.ILoggerService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.endpoint.AbstractEndpoint;
import ru.tsc.kirillov.tm.listener.EntityListener;
import ru.tsc.kirillov.tm.listener.OperationEventListener;
import ru.tsc.kirillov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String url =
                String.format(
                        "http://%s:%s/%s?WSDL",
                        propertyService.getServerHost(),
                        propertyService.getServerPort(),
                        endpoint.getClass().getSimpleName()
                );
        Endpoint.publish(url, endpoint);
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void prepareShutdown() {
        loggerService.info("** Сервер Task Manager остановлен **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initJms() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }

    public void run() {
        loggerService.info("** Сервер Task Manager запущен **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initEndpoints();
        initPID();
        backup.start();
        initJms();
    }

}
