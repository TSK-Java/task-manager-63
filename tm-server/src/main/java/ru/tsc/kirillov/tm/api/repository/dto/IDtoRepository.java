package ru.tsc.kirillov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kirillov.tm.dto.model.AbstractDtoModel;

@NoRepositoryBean
public interface IDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {

}
