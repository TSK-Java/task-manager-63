package ru.tsc.kirillov.tm.exception.system;

import ru.tsc.kirillov.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Ошибка! Аргумент не поддерживается.");
    }

    public ArgumentNotSupportedException(final String argName) {
        super("Ошибка! Аргумент `" + argName + "` не поддерживается.");
    }

}
