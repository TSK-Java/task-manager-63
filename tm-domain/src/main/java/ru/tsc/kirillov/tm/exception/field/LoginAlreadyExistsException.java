package ru.tsc.kirillov.tm.exception.field;

public final class LoginAlreadyExistsException extends AbstractFieldException {

    public LoginAlreadyExistsException() {
        super("Ошибка! Логин уже существует в системе.");
    }

    public LoginAlreadyExistsException(final String login) {
        super("Ошибка! Логин `" + login + "` уже существует в системе.");
    }

}
