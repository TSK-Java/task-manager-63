package ru.tsc.kirillov.tm.exception.field;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Ошибка! Логин не задан.");
    }

}
