package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdRequest extends AbstractIdRequest {

    public TaskListByProjectIdRequest(@Nullable final String token, @Nullable final String projectId) {
        super(token, projectId);
    }

}
