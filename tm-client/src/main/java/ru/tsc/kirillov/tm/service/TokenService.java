package ru.tsc.kirillov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.service.ITokenService;

@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
