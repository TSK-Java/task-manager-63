package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskListRequest;
import ru.tsc.kirillov.tm.dto.response.TaskListResponse;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskListListener extends AbstractTaskListListener {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка задач.";
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Список задач]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);
        printTask(response.getTasks());
    }

}
