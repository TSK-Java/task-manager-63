package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationAllCommandListener extends AbstractSystemListCommandListener {

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка команд.";
    }

    @Override
    @EventListener(condition = "@applicationAllCommandListener.getName() == #event.name || @applicationAllCommandListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd.getName());
    }

}
