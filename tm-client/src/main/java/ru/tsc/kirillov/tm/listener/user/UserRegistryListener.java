package ru.tsc.kirillov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserRegistryRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Регистрация пользователя.";
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Регистрация пользователя]");
        System.out.println("Введите логин");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Введите E-mail");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken(), login, password, email);
        getUserEndpoint().registryUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
