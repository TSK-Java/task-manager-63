package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationAllArgumentListener extends AbstractSystemListCommandListener {

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка аргументов.";
    }

    @Override
    @EventListener(condition = "@applicationAllArgumentListener.getName() == #event.name || @applicationAllArgumentListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final Collection<AbstractListener> arguments = getArguments();
        for (final ICommand cmd: arguments)
            System.out.println(cmd.getArgument());
    }

}
