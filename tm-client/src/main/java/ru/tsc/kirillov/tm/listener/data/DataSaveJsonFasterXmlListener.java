package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataJsonFasterXmlSaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataSaveJsonFasterXmlListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-save-json-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из json файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataSaveJsonFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Сохранение состояния приложения из json файла (FasterXML API)]");
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
