package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.service.ILoggerService;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.SystemUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileCommand fileCommand;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;


    private void prepareStartup() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        fileCommand.start();
    }

    private void prepareShutdown() {
        loggerService.info("** Task Manager завершил свою работу **");
        fileCommand.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String firstArg = args[0];
        publisher.publishEvent(new ConsoleEvent(firstArg));
        return true;
    }

    private void close() {
        System.exit(0);
    }

    private void processCommand() {
        try {
            System.out.println("\nВведите команду:");
            @NotNull final String cmdText = TerminalUtil.nextLine();
            publisher.publishEvent(new ConsoleEvent(cmdText));
            System.out.println("[Ок]");
            loggerService.command(cmdText);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[Ошибка]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) close();
        prepareStartup();
        while (true) processCommand();
    }

}
