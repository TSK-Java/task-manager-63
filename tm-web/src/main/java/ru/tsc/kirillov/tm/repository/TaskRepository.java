package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        write(new Task("Проект 1"));
        write(new Task("Проект 2"));
        write(new Task("Проект 3"));
        write(new Task("Проект 4"));
    }

    public void create() {
        write(new Task(("Новая задача: " + System.currentTimeMillis())));
    }

    public void write(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
